<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Todo;
use AppBundle\Form\TodoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class TodoController extends Controller {
    /**
     * @Route("/", name="todo_list")
     */
    public function listAction() {
        $todos = $this->getDoctrine()
        ->getRepository( 'AppBundle:Todo' )
        ->findAll();

        return $this->render( 'todo/index.html.twig', array(
            'todos' => $todos
            ) );
    }

    /**
     * @Route("/todo/create", name="todo_create")
     */
    public function createAction( Request $request ) {
        $todo = new Todo;

        $todo->setCreateDate( new\DateTime( 'now' ) );

        $form = $this->createForm( TodoType::class, $todo );

        $form->handleRequest( $request );

        if ( $form->isSubmitted() && $form->isValid() ) {
            // Get Data
            $todo = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist( $todo );
            $em->flush();

            $this->addFlash(
                'notice',
                'Todo Added'
                );

            return $this->redirectToRoute( 'todo_list' );
        }

        return $this->render( 'todo/create.html.twig', array(
            'form' => $form->createView()
            ) );
    }

    /**
     * @Route("/todo/edit/{id}", name="todo_edit")
     */
    public function editAction( $id, Request $request ) {
        $todo = $this->getDoctrine()
        ->getRepository( 'AppBundle:Todo' )
        ->find( $id );

        $form = $this->createForm( TodoType::class, $todo );

        $form->handleRequest( $request );

        if ( $form->isSubmitted() && $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $todo = $em->getRepository( 'AppBundle:Todo' )->find( $id );

            // Get Data
            $todo = $form->getData();
            $em->persist( $todo );
            $em->flush();

            $this->addFlash(
                'notice',
                'Todo Updated'
                );

            return $this->redirectToRoute( 'todo_list' );
        }

        return $this->render( 'todo/edit.html.twig', array(
            'todo' => $todo,
            'form' => $form->createView()
            ) );
    }

    /**
     * @Route("/todo/details/{id}", name="todo_details")
     */
    public function detailsAction( $id ) {
        $todo = $this->getDoctrine()
        ->getRepository( 'AppBundle:Todo' )
        ->find( $id );

        return $this->render( 'todo/details.html.twig', array(
            'todo' => $todo
            ) );
    }

    /**
     * @Route("/todo/delete/{id}", name="todo_delete")
     */
    public function deleteAction( $id ) {
        $em = $this->getDoctrine()->getManager();
        $todo = $em->getRepository( 'AppBundle:Todo' )->find( $id );

        $em->remove( $todo );
        $em->flush();

        $this->addFlash(
            'notice',
            'Todo Removed'
            );

        return $this->redirectToRoute( 'todo_list' );

    }

}
